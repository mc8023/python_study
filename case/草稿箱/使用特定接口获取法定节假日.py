import json
from urllib import request
import datetime
import sys



dateList = []
weekday = 0
weekdayList = []
weekend_holiday = 0
weekend_holidayList = []

print("############# 欢迎使用工作日计算系统 #############")

start_date = datetime.date(2019, 10, 10)
end_date = datetime.date(2019, 11, 10)

tag_date = start_date
while True:
    if tag_date > end_date:
        break
    dateList.append(datetime.date.strftime(tag_date, "%Y%m%d"))
    tag_date += datetime.timedelta(days=1)

print(str(dateList))
print(",".join(dateList))

date = ",".join(dateList)
server_url = "http://www.easybots.cn/api/holiday.php?d="

vop_response = request.urlopen(server_url + date)

vop_data = json.loads(vop_response.read())

for v in dateList:
    resDate = vop_data[v]
    print(resDate, end=" ")
    if resDate == '0':
        weekday += 1
        weekdayList.append(v)
    elif resDate == '1' or resDate == '2':
        weekend_holiday += 1
        weekend_holidayList.append(v)
    else:
        print("工作日、休假日错误，程序退出！")
        sys.exit()

print("")
print("工作日天数：", weekday)
print("非工作日天数：", weekend_holiday)
# resDate = vop_data[date]
# print(resDate)
# print(type(resDate))

# for key, value in resDate.items():
#     print(key + ':' + value)

# if vop_data[date] == '0':
#     print("this day is weekday")
# elif vop_data[date] == '1':
#     print('This day is weekend')
# elif vop_data[date] == '2':
#     print('This day is holiday')
# else:
#     print("error")
