num = int(input("请输入一个三位数："))

# 分别求出三位数的个位，十位，百位
gw = num % 10
sw = num % 100 // 10
bw = num // 100
print("个位:{}\n十位:{}\n百位:{}".format(gw,sw,bw))
# 定义变量total，保存各位数字立方和
total = gw ^ 3 + sw ^ 3 + bw ^ 3
print("各位立方和为：{}".format(total))
# 用if语句判断条件是否成立，并做出相应的输出
if total == num:
    print("{}是水仙花数".format(num))
else:
    print("{}不是水仙花数".format(num))