from datetime import datetime

# 定义一个str_字符串为2019-09-10 8:10:56
str_ = "2019-09-10 8:10:56"
# 将str_转换为日期函数2019-09-10 8:10:56
str_date = datetime.strptime(str_, "%Y-%m-%d %H:%M:%S")
print(str_date)
# 定义now_变量接收当前的日期时间
now_ = datetime.now()
# 将当前日期时间格式化为——四位的年份/月/日 时:分:秒
date_str = now_.strftime("%Y/%m/%d %H:%M:%S")
print(date_str)

# 得到当前日期时间（两种方法）
print(datetime.now().strftime("%Y%m%d"))
# 得到当前日期
print(datetime.today())
# 得到当前时间

# 得到当前年份用year_变量接收
year = datetime.now().year
# 得到当前月份用month_变量接收
month = datetime.now().month
# 得到当前天用day_变量接收
day = datetime.now().day
# 使用-拼接年月日得到当前日期
print("{}-{}-{}".format(year,month,day))
