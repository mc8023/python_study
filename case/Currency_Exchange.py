"""
货币兑换的服务系统
    1、实现人民币兑换美元的功能

   2、实现美元兑换人民币的功能

   3、实现人民币兑换欧元的功能

   4、1美元=6.72人民币，1人民币=0.13欧元
"""
# 定义存储服务变量
service_menu = {1: "人民币转换美元", 2: "美元转换人民币", 3: "人民币转换欧元", 0: "结束程序"}

while True:
    print("*******欢迎使用货币转换服务系统*******")
    for key, value in service_menu.items():
        print("{0}.{1}".format(key, value))

    # 接收选择的服务
    Your_Choice = int(input("请您选择需要的服务："))

    if Your_Choice == 1:
        # 人民币转换美元
        print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
        print("欢迎使用{0}服务".format(service_menu.get(Your_Choice)))
        your_money = int(input("请输入您需要转换的人民币金额："))
        RMB_to_US = your_money / 6.72  # 计算人民币转换成美元的金额
        print("您需要转换的人民币为：{:,}元\n兑换成美元为：{:0,.2f}$".format(your_money, RMB_to_US))
    elif Your_Choice == 2:
        # 美元转换人民币
        print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
        print("欢迎使用{0}服务".format(service_menu.get(Your_Choice)))
        your_money = int(input("请输入您需要转换的美元金额："))
        US_to_RMB = your_money * 6.72  # 计算美元转换成人民币的金额
        print("您需要转换的美元为：{:,}美元\n兑换成人民币为：{:0,.2f}￥".format(your_money, US_to_RMB))
    elif Your_Choice == 3:
        # 人民币转换欧元
        print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
        print("欢迎使用{0}服务".format(service_menu.get(Your_Choice)))
        your_money = int(input("请输入您需要转换的人民币金额："))
        RMB_to_EUR = your_money * 0.13  # 计算人民币转换成欧元的金额
        print("您需要转换的人民币为：{:,}元\n兑换成欧元为：{:0,.2f}$".format(your_money, RMB_to_EUR))
    elif Your_Choice == 0:
        # 结束程序
        print("感谢您的使用，祝您生活愉快，再见!")
        break
    else:
        print("输入有误,请重新输入!")
    print("==============================")
