import datetime


class holidaySeasonConfig:
    def __init__(self, start_date, end_date, ny):
        self.start_date = start_date
        self.end_date = end_date
        self.ny = ny

    def getHoliday(self):
        """
        配置节假日
        :return:所有节假日,包括周末
        """
        holidayDict = {
            '201909': ["2019-09-13", "2019-09-14", "2019-09-15", "2019-09-21", "2019-09-22", "2019-09-28", "2019-10-01",
                       "2019-10-02", "2019-10-03", "2019-10-04", "2019-10-05", "2019-10-06", "2019-10-07"],
            '201910': ["2019-10-13", "2019-10-19", "2019-10-20", "2019-10-26", "2019-10-27", "2019-11-02", "2019-11-03",
                       "2019-11-09", "2019-11-10"],
            '201911': ["2019-11-10", "2019-11-16", "2019-11-17", "2019-11-23", "2019-11-24", "2019-11-30",
                       "2019-12-01", "2019-12-07", "2019-12-08"]}
        return holidayDict

    def getWorkday(self):
        """
        获取工作日，除掉法定节假日
        :param start_date: 开始日期
        :param end_date: 结束日期
        :param queryYear: 查询的年月
        :return: 工作日
        """
        dateList = []
        tag_date = self.start_date  # 2019-11-10
        HolidayDict = self.getHoliday()[self.ny]
        while True:
            if tag_date > self.end_date:
                break

            if str(tag_date) not in HolidayDict:
                dateList.append(tag_date)
            tag_date += datetime.timedelta(days=1)

        return dateList

    def workingDays(self):
        """
        :return:返回工作天数
        """
        return len(list(self.getWorkday()))

    def metroCard(self):
        """
        :return: 充值金额
        """
        mcList = {}
        day = self.workingDays()
        MY = 6 * day
        CY = 10 * day
        mcList["MY"] = MY  # 我
        mcList["CY"] = CY  # 他
        return mcList


print("############# LOGIN #############")
while True:
    username = input("请输入用户名：")
    password = input("请输入密码：")
    if username == "cc" and password == "80230923":
        print("############# 欢迎使用工作日计算系统 #############")
        ny = input("请输入想要查询的年月(格式：YYYYMM)：")
        startDate = datetime.datetime.strptime(input("请输入开始日期(格式：YYYY-MM-DD)："), '%Y-%m-%d').date()
        endDate = datetime.datetime.strptime(input("请输入结束日期(格式：YYYY-MM-DD)："), '%Y-%m-%d').date()
        workDayList = holidaySeasonConfig(startDate, endDate, ny)
        print("工作日天数：", workDayList.workingDays())
        print("【M】应充值：", workDayList.metroCard()["MY"], "元,实际充值：", workDayList.metroCard()["MS"])
        print("C应充值：", workDayList.metroCard()["CY"], "元,实际充值：", workDayList.metroCard()["CS"])
        print("############# 感谢使用工作日计算系统 #############")
        break
    else:
        print("用户名或密码错误，请重新输入！")
# queryYear = 201909
# startdate = datetime.date(2019, 9, 10)
# enddate = datetime.date(2019, 10, 10)
