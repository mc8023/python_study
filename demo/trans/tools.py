from datetime import datetime


def gen_trans_id(date=None):
    """
    根据传入时间，得到一个唯一的交易流水ID
    :param date:
    :return:交易流程ID
    """
    if date is None:
        date = datetime.now()
