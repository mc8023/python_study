# print("【同一行显示多条语句】")
import sys;

x = 'rundoc';
sys.stdout.write(x + '\n')

print("【等待用户输入】")
# input("\n\n按下enter鍵后退出!")

print("【多个语句构成代码组】缩进相同的一组语句构成一个代码块，我们称之代码组。")
if x:
    print("suite")
elif x == "rundoc":
    print("suite")
else:
    print("suite")

print("【Print 输出】print 默认输出是换行的，如果要实现不换行需要在变量末尾加上 end=""：")
x = "a"
Y = "b"
# 換行輸出
print(x)
print(Y)

# 不換行輸出
print(x,end="")
print(Y,end="")

"""
【命令行参数】
很多程序可以执行一些操作来查看一些基本信息，Python可以使用-h参数查看各参数帮助信息：
"""

