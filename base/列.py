print("【行与缩进】 python最具特色的就是使用缩进来表示代码块，不需要使用大括号 {} 。\n\
缩进的空格数是可变的，但是同一个代码块的语句必须包含相同的缩进空格数。")
num = 1
if num == 1:
    print("输出结果：1")
elif num == 2:
    print("输出结果：2")
else:
    print("输出结果：都不满足")
# print("缩进不一致，编译报错，执行后会导致运行错误")

print("【多行语句】 Python通常是一行写完一条语句，但如果语句很长，我们可以使用反斜杠(\)来实现多行语句")
item_one = "A"
item_two = "B"
item_three = "C"
total = item_one + \
        item_two + \
        item_three
print("输出结果"+total)
