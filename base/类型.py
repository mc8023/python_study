print("【数字类型】 python中数字有四种类型：整数、布尔型、浮点数和复数。\n\
1.int(整数), 如1, 只有一种整数类型int，表示为长整型，没有python2中的Long。 \n\
2.bool(布尔), 如True。 \n\
3.float(浮点数), 如1.23、3E-2 \n\
4.complex(复数), 如1 + 2j、 1.1 + 2.2j \n\
")
zs = 20
br = True
fd = 1.34
fs = 1.2j
print(type(zs))
print(type(br))
print(type(fd))
print(type(fs))

print("【字符串】")
# python中单引号和双引号使用完全相同。
# 使用三引号('''或""")可以指定一个多行字符串。
# 转义符 '\'
# 反斜杠可以用来转义，使用r可以让反斜杠不发生转义。。 如 r"this is a line with \n" 则\n会显示，并不是换行。
# 按字面意义级联字符串，如"this " "is " "string"会被自动转换为this is string。
# 字符串可以用 + 运算符连接在一起，用 * 运算符重复。
# Python 中的字符串有两种索引方式，从左往右以 0 开始，从右往左以 -1 开始。
# Python中的字符串不能改变。
# Python 没有单独的字符类型，一个字符就是长度为 1 的字符串。
# 字符串的截取的语法格式如下：变量[头下标:尾下标:步长]
word = '字符串word'
sentence = "這是一句話。"
paragraph = """這是一個段落，
可以有多行"""
print(word, '\n', sentence, '\n', paragraph)

strVar = "Rundoc"
# 输出字符串
print(strVar)
# 输出第一个到倒数第二个的所有字符
print(strVar[0:-1])
# 输出字符串第一个字符
print(strVar[0])
# 输出从第三个开始到第五个的字符
print(strVar[2:5])
# 输出从第三个开始后的所有字符
print(strVar[2:])
# 输出字符串两次
print(strVar*2)
# 连接字符串
print(strVar+"你好")
print("------------------------------------------")

# 使用反斜杠(\)+n转义特殊字符
print("hello\nRunoob")
# 在字符串前面添加一个 r，表示原始字符串，不会发生转义
print(r'hello\nRunoob')

print("【空行】空行也是程序代码的一部分。")
