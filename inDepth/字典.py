"""
字典是另一种可变容器模型，且可存储任意类型对象。
字典的每个键值(key=>value)对用冒号(:)分割，每个对之间用逗号(,)分割，整个字典包括在花括号({})中 。
键必须是唯一的，但值则不必。
值可以取任何数据类型，但键必须是不可变的，如字符串，数字或元组。
"""
dict1 = {'Alice': '2341', 'Beth': '9102', 'Cecil': '3258'}
dict2 = {'abc': 456}
dict3 = {'abc': 123, 98.6: 37}
dict4 = {'Name': 'python', 'Age': 7, 'Class': 'First'}
"""
访问字典里的值
把相应的键放入到方括号中 注：大小写敏感，找不到程序报错
"""
print("key取值：", dict1["Alice"])

"""
修改字典
向字典添加新内容的方法是增加新的键/值对，修改或删除已有键/值对
"""
dict1["Alice"] = "1008"
print("修改存在的key值：", dict1)
dict1["Angle"] = "1666"
print("新增不存在的key值：", dict1)

"""
删除字典元素
能删单一的元素也能清空字典，清空只需一项操作。
显示删除一个字典用del命令
"""
del dict4["Name"]
print("指定key,删除后的字典：", dict4)
dict5 = dict4.copy()
print("拷贝的新字典：", dict5)
del dict5
# print("删除后的字典：", dict5) 删除的字典已经不存在，程序会报错

"""
字典键的特性
字典值可以是任何的 python 对象，既可以是标准的对象，也可以是用户定义的，但键不行。
两个重要的点需要记住：
1）不允许同一个键出现两次。创建时如果同一个键被赋值两次，后一个值会被记住
2）键必须不可变，所以可以用数字，字符串或元组充当，而用列表就不行
"""
# 1）不允许同一个键出现两次。创建时如果同一个键被赋值两次，后一个值会被记住，前一个值会被覆盖
dict6 = {'Name': 'python', 'Age': 7, 'Name': '小菜鸟'}
print("dict['Name']: ", dict6['Name'])
# 2）键必须不可变，所以可以用数字，字符串或元组充当，而用列表就不行
# dict7 = {['Name']: 'python', 'Age': 7} # 程序报错
# print("dict['Name']: ", dict7['Name'])

"""
字典内置函数&方法
函数：
len(dict) 计算字典元素个数，即键的总数。
str(dict) 输出字典，以可打印的字符串表示。
type(variable) 返回输入的变量类型，如果变量是字典就返回字典类型。

方法：
radiansdict.clear() 删除字典内所有元素
radiansdict.copy() 返回一个字典的浅复制
radiansdict.fromkeys() 创建一个新字典，以序列seq中元素做字典的键，val为字典所有键对应的初始值
    seq -- 字典键值列表。
    value -- 可选参数, 设置键序列（seq）对应的值，默认为 None。
radiansdict.get(key, default=None) 返回指定键的值，如果值不在字典中返回default值
key in dict 如果键在字典dict里返回true，否则返回false
radiansdict.items() 以列表返回可遍历的(键, 值) 元组数组
radiansdict.keys() 返回一个迭代器，可以使用 list() 来转换为列表
radiansdict.setdefault(key, default=None) 和get()类似, 但如果键不存在于字典中，将会添加键并将值设为default
radiansdict.update(dict2) 把字典dict2的键/值对更新到dict里
radiansdict.values() 返回一个迭代器，可以使用 list() 来转换为列表
pop(key[,default]) 删除字典给定键 key 所对应的值，返回值为被删除的值。key值必须给出。 否则，返回default值。
popitem() 随机返回并删除字典中的最后一对键和值。
"""
print(len(dict1))
print(str(dict1))
print(type(dict1))

# radiansdict.clear() 删除字典内所有元素
dict1.clear()
print(dict1)
# radiansdict.copy() 返回一个字典的浅复制
dict1 = dict2.copy()
print(dict1)
# radiansdict.fromkeys() 创建一个新字典，以序列seq中元素做字典的键，val为字典所有键对应的初始值
seq = ('name', 'age', 'sex')
dict8 = dict.fromkeys(seq)
print("新的字典为 : %s" % str(dict8))
dict9 = dict.fromkeys(seq, 10)
print("新的字典为 : %s" % str(dict9))
dict10 = dict.fromkeys(seq, ("python", 10, '女'))  # 特别注意这个
print("新的字典为 : %s" % str(dict10))
# radiansdict.get(key, default=None) 返回指定键的值，如果值不在字典中返回default值
print("值不在字典中返回default值:", dict1.get("java", "null"))
# key in dict 如果键在字典dict里返回true，否则返回false
print("java" in dict1)
# radiansdict.items() 以列表返回可遍历的(键, 值) 元组数组
print("键, 值:", dict1.items())
result = []
for k, v in dict1.items():
    result.append(k)
    result.append(v)
print(result)
# radiansdict.keys() 返回一个迭代器，可以使用 list() 来转换为列表
print("key：", list(dict4.keys()))
# radiansdict.setdefault(key, default=None) 和get()类似, 但如果键不存在于字典中，将会添加键并将值设为default
print(dict1.setdefault("java", 0))
# radiansdict.update(dict2) 把字典dict2的键/值对更新到dict里
print("原dict1：", dict1)
print("原dict4", dict4)
dict4.update(dict1)
print(dict4)
# radiansdict.values() 返回一个迭代器，可以使用 list() 来转换为列表
print("value：", list(dict1.values()))
# pop(key[,default]) 删除字典给定键 key 所对应的值，返回值为被删除的值。key值必须给出。 否则，返回default值。
dict4.pop("Age")
print(dict4)
# popitem() 随机返回并删除字典中的最后一对键和值。
dict4.popitem()
print(dict4)
