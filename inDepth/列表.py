"""
列表的创建
列表是最常用的Python数据类型，它可以作为一个方括号内的逗号分隔值出现。
列表的数据项不需要具有相同的类型。
创建一个列表，只要把逗号分隔的不同的数据项使用方括号括起来即可。
与字符串的索引一样，列表索引从0开始。列表可以进行截取、组合等。
"""
list1 = ['java', "php", "python", "C++", "jquery"]
list2 = [12, 15, 20, 60, 24, 38, 45, 60]
list3 = ['Google', 'fox', 1997, 2000]

'''
列表值获取和截取
'''
print("精确取值：", list1[0])
print("范围取值：", list1[:3])

print("第三个元素为 : ", list3[2])
# 列表赋值
list3[2] = 2001
print("更新后的第三个元素为 : ", list3[2])

"""
删除列表元素
"""
print("原始列表：", list3)
del list3[2]
print("删除后的列表：", list3)

"""
列表脚本操作符
len([1, 2, 3])	3	长度
[1, 2, 3] + [4, 5, 6]	[1, 2, 3, 4, 5, 6]	组合
['Hi!'] * 4	['Hi!', 'Hi!', 'Hi!', 'Hi!']	重复
3 in [1, 2, 3]	True	元素是否存在于列表中
for x in [1, 2, 3]: print(x, end=" ")	1 2 3	迭代
"""
print(len(list2))
print(list1 + list2)
print(list1 * 4)
print("python" in list1)
for i in list1:
    print(i, end="\n")

"""
列表截取与拼接
L[2]	'Tabasco'	读取第三个元素
L[-2]	'Rundoc'	从右侧开始读取倒数第二个元素: count from the right
L[1:]	['Rundoc', 'Tabasco']	输出从第二个元素开始后的所有元素
"""
L = ['Google', 'Rundoc', 'Tabasco']
print(L[2])
print(L[-2])
print(L[1:])

"""
嵌套列表
"""
list4 = [list1, list2]
print(list4)

"""
列表函数&方法
函数：
len(list) 列表元素个数
max(list) 返回列表元素最大值
min(list) 返回列表元素最小值
list(seq) 将元组转换为列表

方法：
list.append(obj) 在列表末尾添加新的对象
list.count(obj) 统计某个元素在列表中出现的次数
list.extend(seq) 在列表末尾一次性追加另一个序列中的多个值（用新列表扩展原来的列表）
list.index(obj) 从列表中找出某个值第一个匹配项的索引位置
list.insert(index, obj) 将对象插入列表
list.pop([index=-1]) 移除列表中的一个元素（默认最后一个元素），并且返回该元素的值
list.remove(obj) 移除列表中某个值的第一个匹配项
list.reverse() 反向列表中元素
list.sort( key=None, reverse=False) 对原列表进行排序  
    key -- 主要是用来进行比较的元素，只有一个参数，具体的函数的参数就是取自于可迭代对象中，指定可迭代对象中的一个元素来进行排序。
    reverse -- 排序规则，reverse = True 降序， reverse = False 升序（默认）。
list.clear() 清空列表
list.copy() 复制列表
"""
print(len(list2))
print(max(list2))
print(min(list2))
sep1 = ("jquery", "java", "C++", "javaScript")
print(list(sep1))

# list.append(obj) 在列表末尾添加新的对象
list1.append("java")
print("列表末尾添加新的对象:", list1)
# list.count(obj) 统计某个元素在列表中出现的次数
print("元素在列表中出现的次数:", list1.count("java"))
# list.extend(seq) 在列表末尾一次性追加另一个序列中的多个值（用新列表扩展原来的列表）
list1.extend(list2)
print("列表末尾一次性追加另一个序列中的多个值:", list1)
# list.index(obj) 从列表中找出某个值第一个匹配项的索引位置
print("第一个匹配项的索引位置:", list1.index("C++"))  # 该方法返回查找对象的索引位置，如果没有找到对象则抛出异常。
# list.insert(index, obj) 将对象插入列表
list1.insert(2, list2)
print("新列表：", list1)
list1.insert(2, "Spring")
print("新列表：", list1)
# list.pop([index=-1]) 移除列表中的一个元素（默认最后一个元素），并且返回该元素的值
list1.pop(1)
print("新列表：", list1)
# list.remove(obj) 移除列表中某个值的第一个匹配项
list1.remove("java")
print("新列表：", list1)
# list.reverse() 反向列表中元素
list1.reverse()
print("新列表：", list1)
# list.sort( key=None, reverse=False) 对原列表进行排序
print("原列表：", list2)
list2.sort()
print("新列表：", list2)
# list.clear() 清空列表
list1.clear()
print("清空列表：", list1)
# list.copy() 复制列表
list1 = list3.copy()
print("新列表：", list1)
