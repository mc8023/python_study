"""
Python 的元组与列表类似，不同之处在于元组的元素不能修改。
元组使用小括号，列表使用方括号。
元组创建很简单，只需要在括号中添加元素，并使用逗号隔开即可。

元组与字符串类似，下标索引从0开始，可以进行截取，组合等。
"""
tup1 = ('java', "php", "python", "C++", "jquery")
tup2 = (12, 15, 20, 60, 24, 38, 45, 60)
tup3 = ('Google', 'fox', 1997, 2000)
"""
创建空元组
"""
tup4 = ()

"""
元组中只包含一个元素时，需要在元素后面添加逗号，否则括号会被当作运算符使用：
"""
tup5 = (40)
print(type(tup5))  # 不加逗号，类型为整型
tup6 = (40,)
print(type(tup6))  # 加逗号，类型为元组

"""
访问元组
元组可以使用下标索引来访问元组中的值
"""
print("精确取值：", tup1[0])
print("范围取值：", tup1[:3])

"""
修改元组
元组中的元素值是不允许修改的，但我们可以对元组进行连接组合
"""
print("新元组：", tup1 + tup2)

"""
删除元组
元组中的元素值是不允许删除的，但我们可以使用del语句来删除整个元组
"""
del tup6
# print("删除后的元组：", tup6) 删除后元组就不存在了，会报错

"""
元组运算符
len((1, 2, 3))	3	计算元素个数
(1, 2, 3) + (4, 5, 6)	(1, 2, 3, 4, 5, 6)	连接
('Hi!',) * 4	('Hi!', 'Hi!', 'Hi!', 'Hi!')	复制
3 in (1, 2, 3)	True	元素是否存在
for x in (1, 2, 3): print (x,)	1 2 3	迭代
"""
print(len(tup1))
print(tup1 + tup2)
print(tup1 * 2)
print("java" in tup1)
for i in tup1:
    print(i, end="\n")

"""
元组索引，截取
L[2]	'Tabasco'	读取第三个元素
L[-2]	'Rundoc'	从右侧开始读取倒数第二个元素: count from the right
L[1:]	['Rundoc', 'Tabasco']	输出从第二个元素开始后的所有元素
"""
L = ['Google', 'Rundoc', 'Tabasco']
print(L[2])
print(L[-2])
print(L[1:])

"""
元组内置函数
len(tuple) 计算元组元素个数。	
max(tuple) 返回元组中元素最大值。
min(tuple) 返回元组中元素最小值。	
tuple(seq) 将列表转换为元组。	
"""
print(len(tup1))
print(max(tup2))
print(min(tup2))
list1 = ["java", "python", "jquery", "CSS"]
tup7 = tuple(list1)
print(tup7)

