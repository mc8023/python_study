"""
集合（set）是一个无序的不重复元素序列。
可以使用大括号 { } 或者 set() 函数创建集合，注意：创建一个空集合必须用 set() 而不是 { }，因为 { } 是用来创建一个空字典。
"""
"""
创建空集合
"""
set1 = set()
set2 = {'apple', 'orange', 'apple', 'pear', 'orange', 'banana'}

"""
集合的基本操作
1、添加元素
语法格式如下：
s.add( x )
还有一个方法，也可以添加元素，且参数可以是列表，元组，字典等,语法格式如下：
s.update( x )
"""
set3 = set({'apple', 'orange', 'apple', 'pear', 'orange', 'banana'})
set3.add("angle")
print(set3)

set3.update([1, 4], [5, 6])
print(set3)

"""
2、移除元素
语法格式如下：
s.remove( x )
此外还有一个方法也是移除集合中的元素，且如果元素不存在，不会发生错误。格式如下所示：
s.discard( x )
我们也可以设置随机删除集合中的一个元素，语法格式如下：
s.pop() 
"""
# set3.remove("1") # 不存在会发生错误
set3.discard("1")
print(set3)
set3.remove(1)
print(set3)
set3.pop()  # 多次执行测试结果都不一样。然而在交互模式，pop 是删除集合的第一个元素（排序后的集合的第一个元素）。
print(set3)

"""
3、计算集合元素个数
语法格式如下：
len(s)
"""
print(len(set3))

"""
4、清空集合
语法格式如下：
s.clear()
"""
set3.clear()
print(set3)

"""
5、判断元素是否在集合中存在
语法格式如下：
x in s
"""
print("orange" in set2)

"""
集合内置方法完整列表
add()	为集合添加元素
clear()	移除集合中的所有元素
copy()	拷贝一个集合
difference()	返回多个集合的差集
difference_update()	移除集合中的元素，该元素在指定的集合也存在。
discard()	删除集合中指定的元素
intersection()	返回集合的交集
intersection_update()	返回集合的交集。
isdisjoint()	判断两个集合是否包含相同的元素，如果没有返回 True，否则返回 False。
issubset()	判断指定集合是否为该方法参数集合的子集。
issuperset()	判断该方法的参数集合是否为指定集合的子集
pop()	随机移除元素
remove()	移除指定元素
symmetric_difference()	返回两个集合中不重复的元素集合。
symmetric_difference_update()	移除当前集合中在另外一个指定集合相同的元素，并将另外一个指定集合中不同的元素插入到当前集合中。
union()	返回两个集合的并集
update()	给集合添加元素
"""
# add()	为集合添加元素
set2.add("apple")
print("新集合：", set2)
# clear()	移除集合中的所有元素
set3.clear()
print("新集合：", set3)
# copy()	拷贝一个集合
set4 = {"java", "python", "apple"}
set5 = set4.copy()
print("新集合：", set5)
# difference()	返回多个集合的差集
print("原set2：", set2)
print("原set4：", set4)
print("原set2与原set4的差集:", set2.difference(set4))
# difference_update()	移除集合中的元素，该元素在指定的集合也存在。
set2.difference_update(set4)
print("移除集合中的元素，该元素在指定的集合也存在：", set2)
# discard()	删除集合中指定的元素
set4.discard("java")
print("删除集合中指定的元素: ", set4)
# intersection()	返回集合的交集
print("原set2：", set2)
print("原set4：", set4)
print("交集：", set2.intersection(set4))
# intersection_update()	返回集合的交集。
set2.intersection_update(set4)
print("交集：", set2)
# isdisjoint()	判断两个集合是否包含相同的元素，如果没有返回 True，否则返回 False。
print(set2.isdisjoint(set4))
# issubset()	判断指定集合是否为该方法参数集合的子集。
print(set2.issubset(set4))
# issuperset()	判断该方法的参数集合是否为指定集合的子集
print(set2.issuperset(set4))
# pop()	随机移除元素
set8 = set4.pop()
print(set8)
# remove()	移除指定元素
print(set4)
set4.remove("apple")
print(set4)
# symmetric_difference()	返回两个集合中不重复的元素集合。
set9 = {"java", "python", 50, 24, '女'}
set10 = {"python", "C++", "24", 24}
print(set9.symmetric_difference(set10))
# symmetric_difference_update()	移除当前集合中在另外一个指定集合相同的元素，并将另外一个指定集合中不同的元素插入到当前集合中。
set11 = {"java", "python", 50, 24, '女'}
set12 = {"python", "C++", "24", 24}
set11.symmetric_difference_update(set12)
print(set11)
# union()	返回两个集合的并集
print(set11.union(set12))
# update()	给集合添加元素
set2.update(["java", "python"], {12, 19})
print(set2)
